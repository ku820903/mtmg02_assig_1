import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

# Set params for use across all graphs
# Uncomment top params for LATEX-like graphs
graph_params = {
    # 'text.latex.preamble': '\\usepackage{gensymb}',
    # 'text.usetex': True,
    'image.origin': 'lower',
    'image.interpolation': 'nearest',
    'font.family': 'serif',
    'figure.figsize': [9, 9],
    'font.size': 18,
    'axes.labelsize': 18,
    'xtick.labelsize': 18,
    'ytick.labelsize': 18,
    "figure.facecolor": 'white',
}
plt.rcParams.update(graph_params)

###############################
##### Coin flip graphs ########
###############################

# Initialise graphing environment
fig, axs = plt.subplots(2,2)

# Define the samples
sample_x_3 = np.array([0, 1, 1, 1, 2, 2, 2, 3])
sample_x_4 = np.array([0, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4])
sample_y_3 = np.array([2, 2, 2, 2, 2, 2, 3, 3])
sample_y_4 = np.array([2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4])

# Group samples together for easier plotting
samples = [[sample_x_3, sample_y_3], [sample_x_4, sample_y_4]]
labels = [["Sample X \n N = 3", "Sample Y \n N = 3"], \
    ["Sample X \n N = 4", "Sample Y \n N = 4"]]
elements = np.linspace(0,4,5)

# Loop over samples, generate plots
for i, N in enumerate(samples):
    for j, sample in enumerate(N):
        # Initialise arrays
        pdf = np.zeros(len(elements))
        cdf = np.zeros_like(pdf)

        # Find the total number of each element in the samples
        for k, element in enumerate(elements):
            pdf[k] = (sample == element).sum() / len(sample)
        cdf = np.cumsum(pdf)

        # Plot pdfs and cdfs
        axs[i,j].plot(elements, pdf, marker='x', label="pdf")
        axs[i,j].plot(elements, cdf, marker='o', label="cdf")
        axs[i,j].set_xlabel("Value")
        axs[i,j].set_ylabel("Probability")
        axs[i,j].legend(title=labels[i][j])

plt.tight_layout()
plt.savefig("coin_flip_graphs.pdf")

###################################
####### Ensemble Agreement ########
###################################

# Setup graphing environment
graph_params = {
    'figure.figsize': [9, 5],
}
plt.rcParams.update(graph_params)
fig, axs = plt.subplots(1,2)

# Define binom parameters
n_vals = [7, 29]
p = 0.5

# Loop over parameters, create pdf, cdf
for i, n in enumerate(n_vals):
    x = np.arange(n+1)

    # Calculate pdf across full param range remembering that the pdf of agreements is twice the binomial
    pdf = 2*stats.binom.pmf(x,n,p)

    # Now, we need the last half of these distributions from the midpoint onwards
    min_val = int((n+1)/2)
    x   = x[min_val:]
    pdf = pdf[min_val:]
    cdf = np.cumsum(pdf)

    # IDK what to do for the survival function bit

    # For n = 7, get the probability of having 6 or more agreements
    if n == 7:
        print(f"Probability of having 6 or more agreements for N=7: {pdf[2] + pdf[3]}")
    
    # For n = 29, get the index of the cdf array which is closest to a cumulative probability of 1/8
    # For the second part of question 2.5
    if n == 29:
        idx = (np.abs(cdf- 1/8)).argmin()
        print(f"Value of N=29 cdf closest to 1/8 is {cdf[idx]} at index {idx}")
        # Plot this on the graph for good measure
        axs[i].axhline(1/8, color='tab:green', label='p = 1/8')

    # Plot results
    axs[i].plot(x, pdf, label='pdf')
    axs[i].plot(x, cdf, label='cdf')
    axs[i].set_xlabel("Value")
    axs[i].set_ylabel("Probability")
    axs[i].legend(title=f'N = {n}')

plt.tight_layout()
plt.savefig("ensemble_agreement.pdf")

######################################
########## White Christmas ###########
######################################

# Set up the graph environments
# Insets in the precip graph, dual axes in the temp graph
graph_params = {
    'figure.figsize': [11, 6],
}
plt.rcParams.update(graph_params)
fig, axs = plt.subplots(1,2)
ax_0_inset = axs[0].inset_axes([0.3, 0.3, 0.65, 0.65])
ax_1_rhs = axs[1].twinx()

####################################
##### Read and manipulate data #####
####################################

# Read in data, seperate into categories
data = np.genfromtxt('christmas.csv', delimiter=',', skip_header=1)
date   = data[:,0]
snow   = data[:,1]
precip = data[:,2]
temp   = data[:,3]

# Extract the data from the days with snow only 
snow_day_idxs = np.where(snow)
precip_snow_day = np.sort(precip[snow_day_idxs])
temp_snow_day = np.sort(temp[snow_day_idxs])

# Extract the precipitation data which has nonzero elements only
precip_nonzero = precip[precip!=0]
precip_snow_nonzero = precip_snow_day[precip_snow_day!=0]

# Extract temperature data which is below freezing
temp_below_zero = temp[temp<0]
temp_below_zero_snow = temp_snow_day[temp_snow_day<0]

##########################################
##### Print statistics about samples #####
##########################################

# Print out stats about the samples
print(f"Total number of days: {len(date)}")
print(f"Number of snow days: {len(precip_snow_day/len(precip))} out of {len(precip)}")
print(f"Number of days with rain: {len(precip_nonzero)}")
print(f"Number of snow days with rain: {len(precip_snow_nonzero)}")
print(f"Number of days below freezing: {len(temp_below_zero)}")
print(f"Number of snow days below freezing: {len(temp_below_zero_snow)}")

prob_of_precip = (len(precip_nonzero) - 1)/len(precip)
prob_of_freezing = len(temp_below_zero)/len(temp)

print(f"Probability of precipitation: {prob_of_precip}")
print(f"Probability of freezing: {prob_of_freezing}")

# Loop over data to find total number of days which had rain AND were below freezing
# to generate the conditional probabilities
p_and_t = 0

for i, (p, t) in enumerate(zip(precip, temp)):
    if p > 0 and t < 0:
        p_and_t += 1

prob_of_precip_and_freezing = p_and_t/len(precip)

print(f"Probability of precipitation and freezing: {prob_of_precip_and_freezing}")
print(f"Probability of freezing given precip: \
    {prob_of_precip_and_freezing/prob_of_precip}")
print(f"Probability of precip given freezing: \
    {prob_of_precip_and_freezing/prob_of_freezing}")

# Get stats about samples distributions and print results
precip_full_stats = stats.describe(precip)[1:]
precip_snow_stats = stats.describe(precip_snow_day)[1:]
precip_full_nonzero_stats = stats.describe(precip_nonzero)[1:]
precip_snow_nonzero_stats = stats.describe(precip_snow_nonzero)[1:]
temp_full_stats = stats.describe(temp)[1:]
temp_snow_stats = stats.describe(temp_snow_day)[1:]

for name, var in [("precip full", precip_full_stats), ("precip snow", precip_snow_stats), \
    ("precip full nonzero", precip_full_nonzero_stats), \
    ("precip snow nonzero", precip_snow_nonzero_stats), \
    ("temp full", temp_full_stats), ("temp snow", temp_snow_stats),]:
    print(f"{name}: minmax: {var[0]}, mean: {var[1]}, var: {var[2]}, \
        skew: {var[3]}, kurt: {var[4]}")

###########################
##### Populate graphs #####
###########################
# Define bins to use in histograms
bin_width_precip = 5
bin_width_precip_inset = 0.1
bin_width_temp = 1
bins_precip = np.arange(0, 175, bin_width_precip)
bins_precip_inset = np.arange(0, 175, bin_width_precip_inset)
bins_temp = np.arange(-8.5, 13, bin_width_temp)

######## Left Graph #######
# Plot precip hists across full range and zoomed into first 1 mm
axs[0].hist(precip, bins_precip, label=f"Full data")
axs[0].hist(precip_snow_day, bins_precip, label=f"Snow day")
ax_0_inset.hist(precip_nonzero, bins_precip_inset)
ax_0_inset.hist(precip_snow_nonzero, bins_precip_inset)

# Add labels, legends to graph
ax_0_inset.set_xlim(0,1)
axs[0].set_xlabel("Precipitation rate (mm per 24hr)")
axs[0].set_ylabel("Count")
axs[0].legend(title= f"Bin width = {bin_width_precip}", 
              bbox_to_anchor=(0.5, 1.15), ncol=2, loc="center")
ax_0_inset.legend(title=f" Precip $>$ 0 only \n Bin width = {bin_width_precip_inset}")

###### Right graph ######
# PLot temp hists and Gaussian pdfs
axs[1].hist(temp, bins_temp, label=f"Full data")
axs[1].hist(temp_snow_day, bins_temp, label=f"Snow day")
x_01 = np.arange(-12, 12, 0.1)
x_11 = np.arange(-12, 12, 0.1)
ax_1_rhs.plot(x_01, stats.norm.pdf(x_01, temp_full_stats[1], temp_full_stats[2]), 
              color='tab:green', label="Full norm", lw=2)
ax_1_rhs.plot(x_11, stats.norm.pdf(x_11, temp_snow_stats[1], temp_snow_stats[2]), 
              color='tab:red', label="Snow norm", lw=2)

# Dashed line for the center of the pdfs
axs[1].axvline(temp_full_stats[1], ls='--', color='tab:green', lw=2)
axs[1].axvline(temp_snow_stats[1], ls='--', color='tab:red', lw=2)

# Add labels, legends to graph
lines, labels = axs[1].get_legend_handles_labels()
lines2, labels2 = ax_1_rhs.get_legend_handles_labels()
ax_1_rhs.legend(
    lines + lines2, 
    labels + labels2, 
    bbox_to_anchor=(0.5, 1.2), 
    ncol=2, 
    title=f"Bin width = {bin_width_temp}", 
    loc="center"
)
axs[1].set_xlabel("Min Temperature ($^\circ$C)")
axs[1].set_ylabel("Count")
ax_1_rhs.set_ylabel("Probability")

plt.tight_layout()
plt.savefig("snow_day_graphs.pdf")